# File: main_emulate.py

import flask
from fire.main import *

app = flask.Flask('functions')
methods = ['GET', 'POST', 'PUT', 'DELETE']


@app.route('/movies', methods=methods)
@app.route('/movies/<path>', methods=methods)
def catch_all(path=''):
    print("Catch_all is called")
    flask.request.path = '/' + path
    return movies(flask.request)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001, debug=True)
