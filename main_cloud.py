
from flask import jsonify
import firebase_admin
from firebase_admin import firestore, credentials
from firebase_admin import exceptions

cred = credentials.ApplicationDefault()

firebase_admin.initialize_app(cred, {
    'projectId': "authenticate-faa20"
})

# firebase_admin.initialize_app()

db = firestore.client()
COLLECTION = 'movies'

# doc_ref = db.collection('movies').document('LOTR')
# doc_ref.set({
#     'name': 'lord of the ring',
#     'year': '2000',
#     'Genre': 'Fantasy',
# })
#
# doc_ref = db.collection('movies').document('Matrix')
# doc_ref.set({
#     "name": "Matrix",
#     "year": "1999",
#     "Genre": "Sci-Fi",
# })
#
# movie_ref = db.collection('movies')
# movie_docs = movie_ref.stream()
#
# for doc in movie_docs:
#     print("id {} , doc {}".format(doc.id, doc.to_dict()))
#

def create_movie(request):
    print("creat is called")
    db_ref = db.collection(COLLECTION).document(request.form['name'])
    db_ref.set({
        "name": request.form['name'],
        "genre": request.form['genre'],
        "year": request.form['year']
    })
    return str("db_ref is done")


def read_movie(id):
    db_ref = db.collection(COLLECTION).document(id)
    try:
        doc = db_ref.get()
        print("DOC in read():  {}".format(doc.to_dict()))
        if doc is None:
            return "doc doesn't exist"
        return doc.to_dict()
    except exceptions.NotFoundError as e:
        print("The doc doesn't exist")
        return jsonify(str(e))


# def update_movie(id, request):
#     hero = SUPERHEROES.child(id).get()
#     if not hero:
#         return 'Resource not found', 404
#     req = request.json
#     SUPERHEROES.child(id).update(req)
#     return flask.jsonify({'success': True})


def delete_movie(id):
    try:
        db_ref = db.collection(COLLECTION).document(id)
        doc = db_ref.delete()
        print("doc deleted", doc)
        return jsonify({"success": True})
    except Exception as e:
        print("The doc doesn't exist")
        return jsonify(str(e))


def movies(request):

    print("movies called...")
    print("path: ", request.path)
    print("------------------------")

    if request.path == '/' or request.path == '':
        if request.method == 'POST':
            return create_movie(request)
        else:
            return 'Method not supported', 405
    if request.path.startswith('/'):
        id = request.path.lstrip('/')
        if request.method == 'GET':
            return read_movie(id)
        elif request.method == 'DELETE':
            return delete_movie(id)
        # elif request.method == 'PUT':
        #     return update_movie(id, request)
        else:
            return 'Method not supported', 405
    return 'URL not found', 404
