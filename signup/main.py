import firebase_admin
from flask import jsonify
from firebase_admin import credentials
from firebase_admin import firestore
from firebase_admin import auth

cred = credentials.Certificate("./authenticate-service.json")
firebase_admin.initialize_app(cred, {
    "projectId": "authenticate-faa20",
})

db = firestore.client()
COLLECTION = 'users'


def store_user(display_name, email, gender, uid, link):
    print("adding user info to the db...")
    db_ref = db.collection(COLLECTION).document(uid)
    db_ref.set({
        'uid': uid,
        'userName': display_name,
        'email': email,
        'verificationLink': link,
        'gender': gender,
    })


def user(request):
    if request.method == 'POST':
        name = request.form.get("name")
        email = request.form.get("email")
        paswd = request.form.get("password")
        gender = request.form.get("gender")

        usr = auth.create_user(display_name=name, email=email, password=paswd)
        # link = auth.generate_email_verification_link(email)
        store_user(usr.display_name, usr.email, gender, usr.uid, None)

        # token = auth.create_custom_token(usr.uid)
        # print("uid: ", type(token), token)
        # return token
        return jsonify({'success': 200, 'uid': usr.uid})