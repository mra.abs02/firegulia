import getpass
from firebase_admin import credentials
import firebase_admin
from firebase_admin import auth

cred = credentials.Certificate("./authenticate-service.json")
firebase_admin.initialize_app(cred)

dis = input("Display Name: ")
em = input("enter your email: ")
pwd = getpass.getpass("Enter your password: ")

user = auth.create_user(display_name=dis, email=em, password=pwd)
link = auth.generate_email_verification_link(em)

print(link)
print(user.email, user.display_name)
