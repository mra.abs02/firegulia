# this app connects to firestore

from flask import jsonify
import firebase_admin
from firebase_admin import firestore, credentials
from firebase_admin import exceptions

cred = credentials.Certificate("./authenticate-service.json")

firebase_admin.initialize_app(cred, {
"projectId": "authenticate-faa20"
})

# firebase_admin.initialize_app()

db = firestore.client()
COLLECTION = 'movies'

# doc_ref = db.collection('movies').document('LOTR')
# doc_ref.set({
#     'name': 'lord of the ring',
#     'year': '2000',
#     'Genre': 'Fantasy',
# })
#
# doc_ref = db.collection('movies').document('Matrix')
# doc_ref.set({
#     "name": "Matrix",
#     "year": "1999",
#     "Genre": "Sci-Fi",
# })
#
# movie_ref = db.collection('movies')
# movie_docs = movie_ref.stream()
#
# for doc in movie_docs:
#     print("id {} , doc {}".format(doc.id, doc.to_dict()))
#


def get_all():
    print("getting all the docs")
    db_ref = db.collection(COLLECTION).stream()
    # it is better if we paginate this function
    ## we have to convert this generator in our format if we want to send them all to the client
    all = [c.to_dict() for c in db_ref]
    print(all)
    return jsonify(all)


def create_movie(request):
    print("creat is called")
    print("name: ", request.form['name'], "desc: ", request.form['desc'], "year: ", request.form['year'])
    db_ref = db.collection(COLLECTION).document()
    db_ref.set({
        "name": request.form['name'],
        "desc": request.form['desc'],
        "year": request.form['year']
    })
    print(db_ref.id)
    new_movie = {"objectID": db_ref.id}
    # js =
    data = db_ref.get().to_dict()
    new_movie.update(data)

    return jsonify(new_movie)


def read_movie(id):
    print("Read_movie is called...")
    db_ref = db.collection(COLLECTION).document(id)
    try:
        doc = db_ref.get()
        print("DOC in read():  {}".format(doc.to_dict()))
        if doc is None:
            return "doc doesn't exist"
        return jsonify(doc.to_dict())
    except exceptions.NotFoundError as e:
        print("The doc doesn't exist")
        return jsonify(str(e))


# def update_movie(id, request):
#     hero = SUPERHEROES.child(id).get()
#     if not hero:
#         return 'Resource not found', 404
#     req = request.json
#     SUPERHEROES.child(id).update(req)
#     return flask.jsonify({'success': True})


def delete_movie(id):
    try:
        db_ref = db.collection(COLLECTION).document(id)
        doc = db_ref.delete()
        print("doc deleted", doc)
        return jsonify({"success": True})
    except Exception as e:
        print("The doc doesn't exist")
        return jsonify(str(e))


def movies(request):
    print("movies called...")
    print("METHOD: ", request.method)
    # print("name: ", request.form['name'], "desc: ", request.form['desc'], "year: ", request.form['year'])
    print("------------------------")

    if request.path == '/' or request.path == '':
        print("Calling root methods...")
        if request.method == 'POST':
            return create_movie(request)
        elif request.method == 'GET':
            return get_all()
        else:
            return 'Method not supported', 405

    if request.path.startswith('/'):
        print("Calling Param methods...")
        id = request.path.lstrip('/')
        print("Get is calling...")
        if request.method == 'GET':
            return read_movie(id)
        elif request.method == 'DELETE':
            return delete_movie(id)
        # elif request.method == 'PUT':
        #     return update_movie(id, request)
        else:
            return 'Method not supported', 405
    return 'URL not found', 404