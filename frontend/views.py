from concurrent.futures import ThreadPoolExecutor
import json
from flask import Flask, render_template, request, redirect, url_for
import requests
import pyrebase
from algoliasearch.search_client import SearchClient

# initializing the flask app
app = Flask(__name__)

# initializing firebase
user_ser = "https://us-central1-newcloudfunction.cloudfunctions.net/user"
MOVIE_URL = "https://us-central1-newcloudfunction.cloudfunctions.net/movies"

cred = {
    'apiKey': "AIzaSyAwCgKXTvOBXVgDLfvJhpYeNPymqjmjXHg",
    'authDomain': "authenticate-faa20.firebaseapp.com",
    'databaseURL': "https://authenticate-faa20.firebaseio.com",
    'projectId': "authenticate-faa20",
    'storageBucket': "authenticate-faa20.appspot.com",
    'messagingSenderId': "26810792980",
    'appId': "1:26810792980:web:8e502816f5e9b49d4ebb01",
    'measurementId': "G-1MXX7HGRX5",
}

firebase = pyrebase.initialize_app(cred)
auth = firebase.auth()
#------------

# initializing Algolia
client = SearchClient.create("5PUJYS7TP6","69962ba67f145becdb2d66964476fdc4")
index = client.init_index("dev_movies")

def save_to_agolia(doc):
    print("Adding the docs: ", doc.text)
    # it is assumed that only one object is being added. otherwise use save_objects()
    index.save_object(json.loads(doc.text), {'autoGenerateObjectIDIfNotExist': True})

@app.route("/")
def home():
    return render_template('login.html')


@app.route("/login", methods=["POST"])
def login():
    if request.method == "POST":
        try:
            login = auth.sign_in_with_email_and_password(request.form.get('email'), request.form.get('password'))
        except Exception as e:
            print("Error in login: ", e)
            return "Wrong email or password..."
        print(login)

        account = auth.get_account_info(login['idToken'])
        if account['users'][0]['emailVerified'] is False:
            return "Please verify you email."

        return redirect(url_for("dashboard"))


@app.route("/dashboard")
def dashboard():
    req_obj = requests.get(MOVIE_URL)
    # print(req_obj.content, type(req_obj.content))
    return render_template("dashboard.html", records=req_obj.json())


@app.route("/add", methods=['POST', 'GET'])
def add_movie():
    if request.method == 'GET':
        return render_template('add.html')
    else:
        movie = {
            "name": request.form.get("name"),
            "year": request.form.get("year"),
            "desc": request.form.get("desc"),
        }
        req_obj = requests.post(MOVIE_URL, data=movie) ## for sending the form data. data argument should be used not json.
        print("ADD MOVIE: ", )
        # To decide on the number of workers
        with ThreadPoolExecutor(max_workers=1) as executor:
            executor.submit(save_to_agolia, req_obj)
        return redirect(url_for("dashboard"))


@app.route("/reg")
def register_page():
    return render_template('singup.html')\


@app.route("/signup", methods=['POST'])
def signup():
    if request.method == 'POST':
        data={
            'name': request.form.get('name'),
            'email': request.form.get("email"),
            'password': request.form.get("password"),
            'gender': request.form.get("gender"),
        }
        req_obj = requests.post(user_ser, data=data)
        # login = auth.create_user_with_email_and_password(data['email'], data['password'])
        if req_obj.status_code == 200:
            sing_in = auth.sign_in_with_email_and_password(data['email'], data['password'])
            print(sing_in)
            auth.send_email_verification(sing_in['idToken'])
            print("Email verification is sent to the user")
            return redirect(url_for('home'))
        else:
            return "Error in creating the user"


@app.route('/search', methods=['POST', 'GET'])
def search():
    if request.method == 'POST':
        result = index.search(request.form.get('kw'))
        print("Search results:\n", result)
        return render_template("search.html", records=result)
        # return result
    else:
        pass


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)